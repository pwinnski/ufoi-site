import React from 'react';
import { useBaseUrlUtils } from '@docusaurus/useBaseUrl';
import { useColorMode } from '@docusaurus/theme-common';

function SiteLogo(props) {
  const { withBaseUrl } = useBaseUrlUtils();
  const { colorMode } = useColorMode();

  return (
    <img src={withBaseUrl(
                    `img/${
                      colorMode === 'dark'
                        ? 'logo-white-text'
                        : 'logo-text'
                    }.png`
                  )}
                  alt="United Federation of Instances"></img>
  )
}

export default SiteLogo;
